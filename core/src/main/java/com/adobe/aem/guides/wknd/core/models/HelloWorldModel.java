/*
 *  Copyright 2015 Adobe Systems Incorporated
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.adobe.aem.guides.wknd.core.models;

// import static org.apache.sling.api.resource.ResourceResolver.PROPERTY_RESOURCE_TYPE;

import javax.annotation.PostConstruct;

import org.apache.sling.api.resource.Resource;
// import org.apache.sling.api.resource.ResourceResolver;
// import org.apache.sling.models.annotations.Default;
import org.apache.sling.models.annotations.Model;
// import org.apache.sling.models.annotations.injectorspecific.InjectionStrategy;
// import org.apache.sling.models.annotations.injectorspecific.OSGiService;
// import org.apache.sling.models.annotations.injectorspecific.SlingObject;
// import org.apache.sling.models.annotations.injectorspecific.ValueMapValue;

// import com.day.cq.wcm.api.Page;
// import com.day.cq.wcm.api.PageManager;

// import java.util.Optional;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.sling.models.annotations.DefaultInjectionStrategy;

@Model(adaptables = Resource.class, defaultInjectionStrategy = DefaultInjectionStrategy.OPTIONAL)
public class HelloWorldModel {
    @Inject
    @Named("jcr:title")
    private String title;

    @Inject
    // @Named("jcr:title")
    private String text;

    @Inject
    // @Named("customDescription")
    private String customDescription;

    @PostConstruct
    protected void init() {

        // TO do     // first check if the  customDescription is not null and is not
        // empty    
        if (customDescription != null && !customDescription.isEmpty()) {
            // If it not null use the to upperCase method to convert it  
            customDescription = customDescription.toUpperCase();
        } else {
            // if the customDescription is null or empty set the default    
            customDescription = "Not Available";
        }
        if (text != null && !text.isEmpty()) {
            // If it not null use the to upperCase method to convert it  
            text = text.toUpperCase();
        } else {
            // if the customDescription is null or empty set the default    
            text = "slider text 1";
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String gettext(String text) {
        return title;
    }

    public void settext(String text) {
        this.text = text;
    }

    public String getCustomDescription() {
        return customDescription;
    }

    public void setCustomDescription(String customDescription) {
        this.customDescription = customDescription;
    }
}
// @Model(adaptables = Resource.class)
// public class HelloWorldModel {

// @ValueMapValue(name = PROPERTY_RESOURCE_TYPE, injectionStrategy =
// InjectionStrategy.OPTIONAL)
// @Default(values = "No resourceType")
// protected String resourceType;

// @SlingObject
// private Resource currentResource;
// @SlingObject
// private ResourceResolver resourceResolver;

// private String message;

// @PostConstruct
// protected void init() {
// PageManager pageManager = resourceResolver.adaptTo(PageManager.class);
// String currentPagePath = Optional.ofNullable(pageManager)
// .map(pm -> pm.getContainingPage(currentResource))
// .map(Page::getPath).orElse("");

// message = "Hello World!\n"
// + "Resource type is: " + resourceType + "\n"
// + "Current page is: " + currentPagePath + "\n";
// }

// public String getMessage() {
// return message;
// }

// }
